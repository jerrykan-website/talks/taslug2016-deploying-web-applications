Deploying Web Applications: Reproducibility & Isolation
=======================================================

Slides
------
The talk slides were created using reveal.js[1] can be viewed by opening
`index.html` in a browser.


Talk Notes
----------
The talk notes can be found in the `talk_notes.txt` file. The titles for each
section should roughly match up with the slides in `index.html`.


Demos
-----
The "live" demos were recorded using playitagainsam[2], a tool to  record and
replay interactive terminal sessions.

The demos can by played backed using the following commands:

1. Set up a virtualenv ot install playitagainsam in:

  $ virtualenv venv
  $ source venv/bin/activate

2. Install the dependencies

  $ pip install -r requirements.txt

3. Run the demos:

  $ pias sessions/debian_pull_run.talk  # Demo: pull/run docker container
  $ pias sessions/webapp_local.talk     # Demo: Local Webapp
  $ pias sessions/webapp_docker.talk    # Demo: Docker Webapp


[1] https://github.com/hakimel/reveal.js
[2] https://github.com/rfk/playitagainsam
