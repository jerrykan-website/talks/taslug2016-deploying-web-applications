from os import environ

def get_env_settings(setting):
    try:
        return environ[setting]
    except KeyError:
        error_msg = "Set the %s env variable" % setting
        raise EnvironmentError(error_msg)

SECRET = get_env_settings('WEBAPP_SECRET')
