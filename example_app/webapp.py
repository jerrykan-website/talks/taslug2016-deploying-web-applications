import socket
import config
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def main():
    return """\
Hello TasLUG!

From: {}
Listening on port: {}
The secret is: {}
""".format(socket.gethostname(), request.environ['SERVER_PORT'],
           config.SECRET)
